SUMMARY = "Wt is a C++ library for developing web applications"
HOMEPAGE = "http://www.webtoolkit.eu/wt"
DEPENDS = "boost openssl sqlite3 zlib"

LICENSE = "GPL-2.0-with-OpenSSL-exception"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-with-OpenSSL-exception;md5=d9e4412f125e3e6f14efba8ce7b4604f"

inherit cmake

SRCREV = "62b2e6327784e5a514b96487bee8b569252725de"

SRC_URI  = "git://github.com/kdeforche/wt.git"

S = "${WORKDIR}/git"

do_install_append() {
    install -d ${D}${sysconfdir}/wt
    install -m 0644 ${B}/wt_config.xml ${D}${sysconfdir}/wt/
}

FILES_${PN} += "${prefix}"
