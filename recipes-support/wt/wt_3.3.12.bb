SUMMARY = "Wt is a C++ library for developing web applications"
HOMEPAGE = "http://www.webtoolkit.eu/wt"
DEPENDS = "boost openssl sqlite3 zlib"

LICENSE = "GPL-2.0-with-OpenSSL-exception"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-with-OpenSSL-exception;md5=d9e4412f125e3e6f14efba8ce7b4604f"

inherit cmake

BRANCH = "wt3"

SRCREV = "172acb10482fbb1c293da99c6da67fcb7a2cc05d"

SRC_URI  = "git://github.com/kdeforche/wt.git;branch=${BRANCH}"

S = "${WORKDIR}/git"

do_install_append() {
    install -d ${D}${sysconfdir}/wt
    install -m 0644 ${B}/wt_config.xml ${D}${sysconfdir}/wt/
}

FILES_${PN} += "${prefix}"
