SUMMARY = "Virtual Keyboard using jQuery"
DESCRIPTION = "A jQuery on-screen keyboard (OSK) plugin that works in the browser."
HOMEPAGE = "https://github.com/Mottie/Keyboard"
BUGTRACKER = "https://github.com/Mottie/Keyboard/issues"
SECTION = "libs"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=5a30c9cb594ac3dfdada6e8ef68d89d6"

SRC_URI = "https://github.com/Mottie/Keyboard/archive/v${PV}.tar.gz"
SRC_URI[md5sum] = "51a28ca3957b4dd868feb0ab844a6567"
SRC_URI[sha256sum] = "6725bc5315074cf42ea63f964cfe9ce139b48689923a040b47a736f1d9cb337c"

S="${WORKDIR}/Keyboard-${PV}"

INSTALL_PATH="/var/www/keyboard"

INSTALL_DIR="${D}/${INSTALL_PATH}"

do_install(){
	install -d ${INSTALL_DIR} ${INSTALL_DIR}/css ${INSTALL_DIR}/js

	install -m 0400 ${S}/js/jquery.keyboard.js        ${INSTALL_DIR}/js
	install -m 0400 ${S}/docs/js/jquery-latest.min.js ${INSTALL_DIR}/js

	install -m 0400 ${S}/css/keyboard.css             ${INSTALL_DIR}/css
	install -m 0400 ${S}/docs/css/jquery-ui.min.css   ${INSTALL_DIR}/css
}

FILES_${PN} = "${INSTALL_PATH}"
